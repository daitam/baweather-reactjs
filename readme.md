## Weather data web application

ReactJS based web application retrieves weather data by device location or by provided city.

## Setting up

Assuming you have NodeJS and NPM installed already:

1. Clone or download the repository
2. Run `npm install` from terminal at project root and Wait for dependencies to download
3. Run `npm start` to fire up a web server
4. If no errors appear then your application is server at http://localhost:8080

Run `npm run build` to compile CSS and JS files.

## Stack
- ReactJS + Redux
- Webpack
- Babel
- SASS
- OpenWeatherMap API https://openweathermap.org/
- RESTCountries API https://restcountries.eu

## Code quality
- JavaScript linted with ESLint using Airbnb rules.
- SASS linted with Node-only SASS linter.

## Known bugs
- Got into problems while trying to minify JS and CSS using Webpack v1. Should migrate to newest Wepback.