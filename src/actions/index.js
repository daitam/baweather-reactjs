import axios from 'axios';

const WEATHER_API_KEY = '9edc4075cf4fc94896e2fc5085e327df';
const WEATHER_ROOT_URL = `https://api.openweathermap.org/data/2.5/weather?units=metric&appid=${WEATHER_API_KEY}`;

const COUNTRIES_URL = 'https://restcountries.eu/rest/v2/all';

export const FETCH_COUNTRIES = 'fetch_countries';
export const FETCH_WEATHER = 'fetch_weather';
export const FETCH_WEATHER_AT_LOCATION = 'fetch_weather_at_location';

/**
 * Get a detailed list of countries from REST Countries API.
 */
export function fetchCountries() {
  const request = axios.get(COUNTRIES_URL);

  return {
    type: FETCH_COUNTRIES,
    payload: request,
  };
}

/**
 * Get current weather by city and country from OpenWeatherMap API.
 *
 * @param {string} city Name of city to lookup
 * @param {string} country 2 letter country code
 */
export function fetchWeather(city, country) {
  const url = `${WEATHER_ROOT_URL}&q=${city},${country}`;
  const request = axios.get(url);

  return {
    type: FETCH_WEATHER,
    payload: request,
  };
}

/**
 * Get current weather from OpenWeatherMap API at provided coordinates.
 *
 * @param {Object} object Location cordinates.
 */
export function fetchWeatherAtLocation({ lat, lon }) {
  const url = `${WEATHER_ROOT_URL}&lat=${lat}&lon=${lon}`;
  const request = axios.get(url);

  return {
    type: FETCH_WEATHER_AT_LOCATION,
    payload: request,
  };
}
