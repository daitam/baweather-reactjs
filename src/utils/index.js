import _ from 'lodash';

/**
 * Key to use for storing/retrieving data from localStorage.
 */
export const LS_KEY = 'recentLocations';

/**
 * Push to weather search history.
 *
 * @param {string} city Name of city to lookup
 * @param {string} country 2 letter country code
 */
export function appendToLocalStorage(city, country) {
  let existing = localStorage.getItem(LS_KEY);

  if (existing) {
    existing = JSON.parse(existing);

    if (existing[country]) {
      existing[country].push(city);
      existing[country] = _.uniq(existing[country]);
    } else {
      existing[country] = [city];
    }

    localStorage.setItem(LS_KEY, JSON.stringify(existing));
  } else {
    localStorage.setItem(LS_KEY, JSON.stringify({ [country]: [city] }));
  }
}

/**
 * Load from weather search history.
 */
export function loadFromLocalStorage() {
  const data = JSON.parse(localStorage.getItem(LS_KEY));
  return data || [];
}


/**
 * Convert number of degrees to human readable direction abbreviation.
 *
 * @param {Number} degrees Degrees clockwise from North direction.
 */
export function convertDegToText(degrees) {
  const val = Math.floor((degrees / 22.5) + 0.5);
  const directions = ['N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW'];
  return directions[(val % 16)];
}
