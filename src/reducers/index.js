import { combineReducers } from 'redux';

import CountriesReducer from './CountriesReducer';
import WeatherReducer from './WeatherReducer';

const rootReducer = combineReducers({
  countries: CountriesReducer,
  weather: WeatherReducer,
});

export default rootReducer;
