import _ from 'lodash';

import { FETCH_COUNTRIES } from '../actions/index';

/**
 * Handle actions for list of countries.
 *
 * @param {Array} state Previous state
 * @param {Object} action Action sent to store
 */
export default function (state = [], action) {
  switch (action.type) {
    case FETCH_COUNTRIES:
      return _.mapKeys(action.payload.data, 'alpha2Code');

    default:
      return state;
  }
}
