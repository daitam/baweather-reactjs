import { FETCH_WEATHER, FETCH_WEATHER_AT_LOCATION } from '../actions/index';

/**
 * Handle actions weather data.
 *
 * @param {Array} state Previous state
 * @param {Object} action Action sent to store
 */
export default function (state = [], action) {
  switch (action.type) {
    case FETCH_WEATHER:
    case FETCH_WEATHER_AT_LOCATION:
      return action.payload.data !== undefined ? action.payload.data : null;

    default:
      return state;
  }
}
