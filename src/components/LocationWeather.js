import moment from 'moment';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import LocationWeatherDetails from './LocationWeatherDetails';
import MapContainer from './MapContainer';

/**
 * Displays weather details and map.
 */
class LocationWeather extends Component {
  constructor(props) {
    super(props);

    this.state = { weather: '' };
  }

  render() {
    // If no weather data loaded
    if (!this.props.weather || !this.props.weather.name) {
      return (
        <div className="location-weather-placeholder">
          <img src="img/cloud.svg" style={{ width: '64px', height: '64px' }} />
          <h3>Get current weather details by city or current location!</h3>
        </div>
      );
    }

    const weatherData = this.props.weather;
    // OpenWeatherMap API provides PNG icon for every weather condition
    const iconUrl = `http://openweathermap.org/img/w/${weatherData.weather[0].icon}.png`;
    const details = <LocationWeatherDetails details={weatherData} />;

    return (
      <div className="location-weather">
        <div className="half">
          <h3>Current weather in {weatherData.name}, {weatherData.sys.country}</h3>
          <small>{moment.unix(weatherData.dt).format('YYYY-MM-DD HH:mm')}</small>

          {details}
        </div>
        <div className="half">
          <MapContainer
            google={window.google}
            info={details}
            lat={weatherData.coord.lat}
            lon={weatherData.coord.lon}
            marker={iconUrl}
            zoom={12}
          />
          <small>Click on the icon for more information</small>
        </div>
      </div>
    );
  }
}

function mapStateToProps({ weather }) {
  return { weather };
}

export default connect(mapStateToProps, null)(LocationWeather);
