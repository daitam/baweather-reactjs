import React from 'react';

import SearchBar from './SearchBar';
import LocationWeather from './LocationWeather';
import RecentLocations from './RecentLocations';
import CurrentLocation from './CurrentLocation';

/**
 * Main component.
 */
export default () => (
  <div>
    <SearchBar />
    <CurrentLocation />
    <LocationWeather />
    <RecentLocations />
  </div>
);
