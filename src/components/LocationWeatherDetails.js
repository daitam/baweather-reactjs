import React from 'react';

import { convertDegToText } from '../utils';

/**
 * Displays a detailed weather info.
 */
export default props => (
  <ul className="location-weather-details">
    <li>
      <span>Temperature:</span> {props.details.main.temp} &#176;C
    </li>
    <li>
      <span>Humidity:</span> {props.details.main.humidity} %
    </li>
    <li>
      <span>Pressure:</span> {props.details.main.pressure} hPa
    </li>
    <li>
      <span>Wind speed:</span> {props.details.wind.speed} m/s
    </li>
    <li>
      <span>Wind direction:</span> {convertDegToText(props.details.wind.deg)}
    </li>
    <li>
      <span>Cloudness:</span> {props.details.clouds.all} %
    </li>
  </ul>
);
