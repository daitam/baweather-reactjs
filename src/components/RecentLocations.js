import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchWeather } from '../actions/index';

import { loadFromLocalStorage } from '../utils';

/**
 * Displays list of recently searched cities.
 */
class RecentLocations extends Component {
  constructor(props) {
    super(props);

    this.state = { locations: loadFromLocalStorage() };
  }

  /**
   * Build a list of recent searches.
   */
  renderLocationLinks() {
    return _.map(this.state.locations, (location, country) => location.map(city => (
      <li
        key={city}
        onClick={() => this.props.fetchWeather(city, country)}
      >
        {_.capitalize(city)}
      </li>
    )));
  }

  render() {
    if (this.state.locations.length === 0) {
      return null;
    }

    return (
      <div className="recent-locations">
        <h3>Recently searched</h3>

        <ul>
          {this.renderLocationLinks()}
        </ul>
      </div>
    );
  }
}

export default connect(null, { fetchWeather })(RecentLocations);
