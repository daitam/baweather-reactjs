import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { appendToLocalStorage } from '../utils';
import { fetchCountries, fetchWeather } from '../actions/index';

/**
 * Displays a component for searching weather information
 * by country and city.
 */
class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = { term: '', country: '' };

    this.onInputChange = this.onInputChange.bind(this);
    this.onOptionChange = this.onOptionChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  /**
   * Get a list of countries from API.
   */
  componentDidMount() {
    this.props.fetchCountries();

    // Autoselect first country
    this.setState({ country: this.props.countries[0] });
  }

  /**
   * Fill dropdown with country names.
   */
  renderCityOptions() {
    return _.map(this.props.countries, (country, index) => (
        <option key={index} value={index.toLowerCase()}>{country.name}</option>
    ));
  }

  onInputChange(event) {
    this.setState({ term: event.target.value });
  }

  onOptionChange(event) {
    this.setState({ country: event.target.value });
  }

  validateForm() {
    return /^([a-z0-9]{5,})$/.test(this.state.term) && this.state.term.length > 0;
  }

  /**
   * Handle search form submit.
   *
   * @param {Event} event Object describing the event that has been fired and needs to be processed.
   */
  onFormSubmit(event) {
    event.preventDefault();

    // Validate input
    if (!this.validateForm()) {
      alert('Invalid search term');
      return;
    }

    // Attempt to get weather data from API
    this.props.fetchWeather(this.state.term, this.state.country)
      .then((response) => {
        if (response) {
          appendToLocalStorage(this.state.term, this.state.country);
        } else {
          alert('Cannot retrieve weather data');
        }

        this.setState({ term: '' });
      });
  }

  render() {
    return (
      <div className="search-bar">
        <form onSubmit={this.onFormSubmit}>
          <select
            value={this.state.country}
            onChange={this.onOptionChange}
          >
            {this.renderCityOptions()}
          </select>

          <input
            type="text"
            placeholder="Enter city name..."
            onChange={this.onInputChange}
            value={this.state.term}
          />

          <button type="submit">Search</button>
        </form>
      </div>
    );
  }
}

function mapStateToProps({ countries }) {
  return { countries };
}

export default connect(mapStateToProps, { fetchCountries, fetchWeather })(SearchBar);
