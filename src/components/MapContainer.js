import React, { Component } from 'react';
import { Map, Marker, InfoWindow } from 'google-maps-react';

/**
 * Displays GoogleMap element.
 */
export default class MapContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showingInfoWindow: false,
      activeMarker: {},
    };
  }

  /**
   * Show info popover when map marker is clicked.
   */
  onMarkerClick = (props, marker) => this.setState({
    activeMarker: marker,
    showingInfoWindow: true,
  });

  render() {
    return (
      <div className="map-container">
        <Map
          style={{ width: '100%', height: '100%', position: 'relative' }}
          google={this.props.google}
          zoom={this.props.zoom}
          initialCenter={{
            lat: this.props.lat,
            lng: this.props.lon,
          }}
          center={{
            lat: this.props.lat,
            lng: this.props.lon,
          }}
        >

          <Marker
            onClick={this.onMarkerClick}
            position={{
              lat: this.props.lat,
              lng: this.props.lon,
            }}
            icon={{
              url: this.props.marker,
              anchor: new this.props.google.maps.Point(32, 32),
              scaledSize: new this.props.google.maps.Size(64, 64),
            }}
          />

          <InfoWindow
            marker={this.state.activeMarker}
            visible={this.state.showingInfoWindow}
          >
            <div>
              {this.props.info}
            </div>
          </InfoWindow>
        </Map>
      </div>
    );
  }
}
