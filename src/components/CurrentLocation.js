import React, { Component } from 'react';
import { connect } from 'react-redux';

import { fetchWeatherAtLocation } from '../actions/index';

/**
 * Displays a link that retrieves weather details at current location.
 */
class CurrentLocation extends Component {
  constructor(props) {
    super(props);

    this.onLocateClick = this.onLocateClick.bind(this);
  }

  /**
   * Attempt to get device location using browser's native Geolocation API.
   * On success - fetch weather details for resolved location.
   */
  onLocateClick() {
    const location = window.navigator && window.navigator.geolocation;

    if (location) {
      // Geolocation API is available
      location.getCurrentPosition((position) => {
        this.props.fetchWeatherAtLocation({
          lat: position.coords.latitude,
          lon: position.coords.longitude,
        });
      }, () => {
        // Handle geolocation attempt error
        const msg = 'Cannot get location data';
        alert(msg);
      });
    }
  }

  render() {
    return (
      <div className="current-location">
        <span onClick={this.onLocateClick}>Show weather at my location</span>
      </div>
    );
  }
}

export default connect(null, { fetchWeatherAtLocation })(CurrentLocation);
