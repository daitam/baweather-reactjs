const path = require('path');
const sassLintPlugin = require('sasslint-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");


module.exports = {
  entry: ['./src/index.js'],
  output: {
    path: __dirname + '/public',
    publicPath: '/',
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets: ['react', 'es2015', 'stage-1']
        }
      },
      // {
      //   test: /\.scss$/,
      //   loaders: [
      //     "style-loader",
      //     "css-loader",
      //     "sass-loader"
      //   ]
      // },
      {
        test: /\.scss/,
        loader: ExtractTextPlugin.extract(
          "style",
          "css!sass")
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint'
      }
    ]
  },
  resolve: {
    extensions: ['', '.js']
  },
  devServer: {
    historyApiFallback: true,
    contentBase: './public/',
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000
    }
  },
  plugins: [
    new sassLintPlugin({
      // configFile: '.sass-lint.yml',
      context: ['inherits from webpack'],
      ignoreFiles: [],
      ignorePlugins: [],
      glob: './src/styles/**/*.s?(a|c)ss',
      quiet: false,
      failOnWarning: false,
      failOnError: false,
      testing: false
    }),
    new ExtractTextPlugin("styles.css")
  ]
};
